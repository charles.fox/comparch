import re
f = open("TuringLongDivision.asm")
for_logisim = True # change to True to output hex for logisim RAM
dct_inst = dict()
dct_inst['JMP'] = 0
dct_inst['JRP'] = 1
dct_inst['LDN'] = 2
dct_inst['STO'] = 3
dct_inst['SUB'] = 4
dct_inst['SKN'] = 6
dct_inst['HLT'] = 7
loc = 0
if for_logisim:
  print("v2.0 raw")    #header for logisim RAM image format
def sext(num, width):
    if num < 0:
        return bin((1 << (width + 1)) + num)[3:]
    return bin(num)[2:].zfill(width)
def out(binary):
  if for_logisim:
    print(hex(int(binary,2))[2:].zfill(8))
  else:
    print(binary[::-1])  #humans like to view bit 0 on the right  
for line in f:
    asm = re.split('\s*--\s*', line.strip())[0]
    parts = asm.split()
    thisloc = int(parts[0][:-1])
    if parts[1] == 'NUM':      #data line
        code2 = sext(int(parts[2], 10), 32)
    else:                      #instruction line
        inst2 = bin(dct_inst[parts[1]]).zfill(3)[2:]
        if len(parts) < 3:
            parts.append('0')
        operand2 = sext(int(parts[2], 10), 13)
        code2 = (inst2 + operand2).zfill(32)
    for addr in range(loc, thisloc):
      out('0'.zfill(32))  #fill in zeros where lines not given
    out(code2)
    loc = thisloc + 1
