# comparch

Code, images, and copies and modifications of open source tools for use with the book <a href="https://nostarch.com/computerarchitecture">Computer Architecture</a>

My <a href="https://www.youtube.com/playlist?list=PLjrJD5nSYNjonK0V57vNutXx-VjyyqVs-">Video lectures</a> on computer architecture


<a href="https://gitlab.com/charles.fox/comparch-figs">Figures</a> 

The Manchester Baby model can be used with <a href="https://github.com/andy-bower/babyutils">Andy Bower's babyutils</a>

My student Harry Fitchett has extended the Baby model to make a <a href="https://github.com/LAMB-TARK/MOS-6502-Logisim-Evolution">complete 6502 in LogiSim</a>.
