import pdb 

import re
f = open("myprog.asm")
dct_inst = dict()
dct_inst['JMP'] = "000"
dct_inst['SUB'] = "001"
dct_inst['LDN'] = "010"
dct_inst['SKN'] = "011"
dct_inst['STO'] = "110"
dct_inst['HLT'] = "111"
words=[]
for line in f:
    line=line.strip()
    line=re.sub("--.*","",line) #remove any comments
    parts = line.split()
    linenum10 = parts[0]
    if parts[1]=='NUM':
        code2=bin(int(parts[2],10))[2:].zfill(32)
    else:
        inst2 = dct_inst[parts[1]]
        if len(parts)<3:
            parts.append('0')
        operand2=(bin(int(parts[2],10))[2:]).zfill(32-3)
        code2 = operand2 + inst2
    code16 = hex(int(code2,2))[2:].zfill(8)
    print(code2)    #change to code16 if you want hex
    words.append(code16)


f = open("myram", "w")
f.write("v3.0 hex words addressed\n")
for l in range(0,32, 8):
    line=hex(l)[2:].zfill(2) +": "
    for i in range(0,8):
        line=line+words[l+i]+" "
    line=line[:-1]+"\n"
    f.write(line)
f.close()
