        processor 6502          ; define processor family for dasm
        org $C000               ; memory location for our code
screenbeg = $0400               ; const, beginning of screen memorymap
screenend = $07e7               ; const, end of screen memorymap
screenpos = $8000               ; variable, current position in screen
main:
	LDA #$02
        STA $d020               ; VDU border color 
        STA $d021               ; VDU background color 
        STA screenpos           ; screen position
loop:				; main game loop, once per frame
        JSR $E544		; ROM routine, clears screen
        JSR drawframe           ; most of the work is done here
	JSR check_keyboard
        INC screenpos           ; increment current screen position
        JMP loop                ; do the loop, forever
drawframe:
        LDX #$00                ; regX tracks idx of char in the string
        LDY screenpos           ; regY keeps scrolling screen position
        CPY #$20		; compare Y with constant 20 
        BCS resetscreenpos      ; branch if Y>20 (stored in carry bit)
drawmsgloop:                    ; drop through to here if not branching
        LDA msg,X               ; load the xth char of the msg
        BEQ return              ; exit when zero char (end of string)
        AND #$3f                ; convert ascii to petscii
        STA screenbeg,Y         ; VDU: write char in A to mmap offset Y
        INX                     ; increment idx of char in message
        INY                     ; increment location on screen
        CPY #$20		; are we trying to write offscreen?
        BCS wraparound_y        ; if so, shift offset by screen width
        JMP drawmsgloop		; loop (until all chars are done)
resetscreenpos:
        LDY #$00
        STY screenpos		; reset the screenpos to 0
        JMP drawmsgloop
wraparound_y:		; if Y trying to write off screen, wrap
        TYA			; Transfer Y to Accumulator
        SBC #$20		; SuBtract with Carry
        TAY			; Transfer Accumulator to Y
        JMP drawmsgloop
check_keyboard:
	JSR $FF9F		; ROM SCANKEY IO, writes keybdcode to 00CB
	JSR $FFE4		; ROM GETIN, writes keycode to acc 
	CMP #65                 ; compare acumultor to ASCII 'A'
	BNE return
	BRK
return:
        RTS
msg:
        .byte "HELLO C64!\0" 	;this is data not an instruction
