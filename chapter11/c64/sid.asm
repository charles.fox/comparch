        processor 6502          ; define processor family for dasm
        org $C000               ; memory location for our code
main:	
	LDA #$0F
	STA $D418  ;IO SID volume

	LDA #$BE   ; attack duration=B, decay duration=E
	STA $D405  ;IO SID attack and decay byte

	LDA #$F8   ; sustain level=F, release duration=8
	STA $D406  ;IO SID sustain and release byte

	LDA #$11   ; frequency high byte = 11
	STA $D401  ;IO SID frequency high byte

	LDA #$25   ; frequency low byte=25
	STA $D400  ;IO SID frequency low byte

	LDA #$11   ; id for square wave waveform
	STA $D404  ;IO SID ctl register 
loop:
	JMP loop
