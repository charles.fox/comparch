custom      equ   $dff000     ; custom chips
bplcon0     equ   $100        ; bitplane control register 0 (misc, control bits)
bplcon1     equ   $102        ; bitplane control register 1 (horizontal, scroll
bplcon2     equ   $104        ; bitplane control register 2 (priorities, misc)
bpl1mod     equ   $108	      ; bitplane modulo
ddfstrt     equ   $092        ; data fetch start
ddfstop     equ   $094        ; data fetch stop
diwstrt     equ   $08E        ; display window start
diwstop     equ   $090        ; display window stop
copjmp1     equ   $088        ; Copper restart at first location
cop1lc      equ   $080        ; Copper list pointer
dmacon      equ   $096        ; DMA controller
sprpt       equ   $120        ; sprite pointer

COLOR00     equ   $180        ; address to store COLOR00 (background)
COLOR01     equ   COLOR00+$02 ; address to store COLOR01 (foreground)
COLOR17     equ   COLOR00+$22 ; etc
COLOR18     equ   COLOR00+$24
COLOR19     equ   COLOR00+$26 

BPL1PTH     equ   $0E0        ; bitplane 1 pointer hi byte
BPL1PTL     equ   BPL1PTH+$02 ; bitplane 1 pointer lo byte
SPR0PTH     equ   sprpt+$00   ; sprite0 pointer, hi byte
SPR0PTL     equ   SPR0PTH+$02 ; sprite0 pointer, lo byte
SPR1PTH     equ   sprpt+$04   ; sprite1 etc
SPR1PTL     equ   SPR1PTH+$02
SPR2PTH     equ   sprpt+$08
SPR2PTL     equ   SPR2PTH+$02
SPR3PTH     equ   sprpt+$0C
SPR3PTL     equ   SPR3PTH+$02
SPR4PTH     equ   sprpt+$10
SPR4PTL     equ   SPR4PTH+$02
SPR5PTH     equ   sprpt+$14
SPR5PTL     equ   SPR5PTH+$02
SPR6PTH     equ   sprpt+$18
SPR6PTL     equ   SPR6PTH+$02
SPR7PTH     equ   sprpt+$1C
SPR7PTL     equ   SPR7PTH+$02

SHIPSPRITE  equ $25000        ; address to store our ship sprite
DUMMYSPRITE equ $30000        ; address to store our dummy sprite
COPPERLIST  equ $20000        ; address to stote our copper list
BITPLANE1   equ $21000 	      ; address to store our bitplane data

; Define bitplane1
        lea     custom,a0               ; a0 := address of custom chips
        move.w  #$1200,bplcon0(a0)      ; 1 bitplane color
        move.w  #$0000,bpl1mod(a0)      ; modulo := 0
        move.w  #$0000,bplcon1(a0)      ; horizontal scroll value := 0
        move.w  #$0024,bplcon2(a0)      ; give sprites priority over playfields
        move.w  #$0038,ddfstrt(a0)      ; data-fetch start
        move.w  #$00D0,ddfstop(a0)      ; data-fetch stop

; Define display window
        move.w  #$3c81,diwstrt(a0)      ; set window start (hi byte=vertical, lo=horiz*2)   
        move.w  #$ffc1,diwstop(a0)      ; set window stop (hi byte=vertical, lo=horiz*2)    
                                        
; Put RGB constants defining colors into the color registers
        move.w  #$000f,COLOR00(a0)      ; set color 00 (background) to blue (00f)
        move.w  #$0000,COLOR01(a0)      ; set color 01 (foreground) to black (000)
        move.w  #$0ff0,COLOR17(a0)      ; Set color 17 to yellow (ff0)
        move.w  #$00ff,COLOR18(a0)      ; Set color 18 to cyan (0ff)
        move.w  #$0f0f,COLOR19(a0)      ; Set color 19 to magenta (f0f)

; Copy Copper list data to addresses starting at COPPERLIST
        move.l  #COPPERLIST,a1          ; a1 := Copper list destination
        lea     copperl(pc),a2          ; a2 := Copper list source
cloop:
        move.l  (a2),(a1)+              ; copy DMA command
        cmp.l   #$fffffffe,(a2)+        ; end of list?
        bne     cloop                   ; loop until whole list moved

; Copy sprite to addresses starting at SHIPSPRITE
        move.l  #SHIPSPRITE,a1          ; a1 := sprite destination
        lea     sprite(pc),a2           ; a2 := sprite source
sprloop:
        move.l  (a2),(a1)+              ; copy DMA command
        cmp.l   #$00000000,(a2)+        ; end of sprite?
        bne     sprloop                 ; loop until whole sprite moved

; All eight sprites are activated at the same time but we will only use one.
; So write a blank sprite to DUMMYSPRITE, so the other sprites can point to it
        move.l  #$00000000,DUMMYSPRITE  
   
; Point Copper at our Copper list data
        move.l  #COPPERLIST,cop1lc(a0)  

gameloop:	

; Fill bitplane pixels with foreground color (1-bit plane in fore/background colors)
        move.l  #BITPLANE1,a1           ; a1 := bitplane
        move.w  #1999,d0                ; 2000-1(for dbf) long words = 8000 bytes
floop:
        move.l  #$ffffffff,(a1)+        ; put bit pattern $ffffffff as next row of 16*8 pixels
        dbf     d0,floop                ; decrement, repeat until false.

; start DMA, to blit the sprite onto the bitplane
        move.w  d0,copjmp1(a0)          ; force load to Copper program counter
        move.w  #$83A0,dmacon(a0)       ; bitplane, Copper, and sprite DMA

	;**your game logic would go here -- read keyboard, move sprites**

	jmp gameloop			

; Copper list for one bitplane, and 8 sprites.  Bitplane is at BITPLANE1.
; Sprite 0 is at SHIPSPRITE; other (dummy) sprites are at DUMMYSPRITE
copperl:
        dc.w    BPL1PTH,$0002           ; bitplane 1 pointer := BITPLANE1
        dc.w    BPL1PTL,$1000
        dc.w    SPR0PTH,$0002           ; sprite 0 pointer := SHIPSPRITE
        dc.w    SPR0PTL,$5000
        dc.w    SPR1PTH,$0003           ; sprite 1 pointer := DUMMYSPRITE
        dc.w    SPR1PTL,$0000
        dc.w    SPR2PTH,$0003           ; sprite 2 pointer := DUMMYSPRITE
        dc.w    SPR2PTL,$0000
        dc.w    SPR3PTH,$0003           ; sprite 3 pointer := DUMMYSPRITE
        dc.w    SPR3PTL,$0000
        dc.w    SPR4PTH,$0003           ; sprite 4 pointer := DUMMYSPRITE
        dc.w    SPR4PTL,$0000
        dc.w    SPR5PTH,$0003           ; sprite 5 pointer := DUMMYSPRITE
        dc.w    SPR5PTL,$0000
        dc.w    SPR6PTH,$0003           ; sprite 6 pointer := DUMMYSPRITE
        dc.w    SPR6PTL,$0000
        dc.w    SPR7PTH,$0003           ; sprite 7 pointer := DUMMYSPRITE
        dc.w    SPR7PTL,$0000
        dc.w    $ffff,$fffe             ; Copper list end

; Sprite data. Stores (x,y) screen coordinate and image data
sprite:
        dc.w    $6da0,$7200             ; 6d=y location; a0=x location; 72-6d=5=height) 
        
        dc.w    $0000,$0ff0             ; Image data, 5rows x 16cols x 2bit color
        dc.w    $0000,$33cc	        ; each line describes one row of 16 pixels
        dc.w    $ffff,$0ff0             ; each pixel is described by a 2-bit color
        dc.w    $0000,$3c3c             ; the low pixel bits form the first word
        dc.w    $0000,$0ff0             ; the high pixel bits form the second word
        
        dc.w    $0000,$0000             ; ... all zeros marks end of image data
