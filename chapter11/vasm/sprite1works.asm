CUSTOM          equ     $DFF000

bplcon0     EQU   $100
bplcon1     EQU   $102
bplcon2     EQU   $104

ddfstrt     EQU   $092
ddfstop     EQU   $094
diwstrt     EQU   $08E
diwstop     EQU   $090

BPLCON0         equ     bplcon0 
BPLCON1         equ     bplcon1
BPLCON2         equ     bplcon2 
DDFSTRT         equ     ddfstrt        
DDFSTOP         equ     ddfstop         
DIWSTRT         equ     diwstrt         ;  "         "
DIWSTOP         equ     diwstop  

color	    EQU   $180
COLOR00         equ     color+$00
COLOR01         equ     color+$02
COLOR02         equ     color+$04
COLOR03         equ     color+$06
COLOR04         equ     color+$08
COLOR05         equ     color+$0A
COLOR06         equ     color+$0C
COLOR07         equ     color+$0E
COLOR08         equ     color+$10
COLOR09         equ     color+$12
COLOR10         equ     color+$14
COLOR11         equ     color+$16
COLOR12         equ     color+$18
COLOR13         equ     color+$1A
COLOR14         equ     color+$1C
COLOR15         equ     color+$1E
COLOR16         equ     color+$20
COLOR17         equ     color+$22
COLOR18         equ     color+$24
COLOR19         equ     color+$26
COLOR20         equ     color+$28
COLOR21         equ     color+$2A
COLOR22         equ     color+$2C
COLOR23         equ     color+$2E
COLOR24         equ     color+$30
COLOR25         equ     color+$32
COLOR26         equ     color+$34
COLOR27         equ     color+$36
COLOR28         equ     color+$38
COLOR29         equ     color+$3A
COLOR30         equ     color+$3C
COLOR31         equ     color+$3E

BPL1MOD         equ     bpl1mod
DMACON          equ     dmacon 


bpl1mod     EQU   $108
copjmp1     EQU   $088
cop1lc	    EQU   $080
dmacon	    EQU   $096
bplpt	    EQU   $0E0
sprpt	    EQU   $120

COP1LC          equ     cop1lc     
COPJMP1         equ     copjmp1
BPL1PTH         equ     bplpt+$00
BPL1PTL         equ     bplpt+$02


SPR0PT          equ     sprpt+$00
SPR0PTH         equ     SPR0PT+$00
SPR0PTL         equ     SPR0PT+$02
SPR1PT          equ     sprpt+$04
SPR1PTH         equ     SPR1PT+$00
SPR1PTL         equ     SPR1PT+$02
SPR2PT          equ     sprpt+$08
SPR2PTH         equ     SPR2PT+$00
SPR2PTL         equ     SPR2PT+$02
SPR3PT          equ     sprpt+$0C
SPR3PTH         equ     SPR3PT+$00
SPR3PTL         equ     SPR3PT+$02
SPR4PT          equ     sprpt+$10
SPR4PTH         equ     SPR4PT+$00
SPR4PTL         equ     SPR4PT+$02
SPR5PT          equ     sprpt+$14
SPR5PTH         equ     SPR5PT+$00
SPR5PTL         equ     SPR5PT+$02
SPR6PT          equ     sprpt+$18
SPR6PTH         equ     SPR6PT+$00
SPR6PTL         equ     SPR6PT+$02
SPR7PT          equ     sprpt+$1C
SPR7PTH         equ     SPR7PT+$00
SPR7PTL         equ     SPR7PT+$02

;
; sprite_display.asm
;
; This example displays the spaceship sprite at location V = 65,
; H = 128. Remember to include the file hw_examples.i.
;
; First, we set up a single bitplane.
;
        LEA     CUSTOM,a0               ;Point a0 at custom chips
        MOVE.W  #$1200,BPLCON0(a0)      ;1 bitplane color is on
        MOVE.W  #$0000,BPL1MOD(a0)      ;Modulo = 0
        MOVE.W  #$0000,BPLCON1(a0)      ;Horizontal scroll value = 0
        MOVE.W  #$0024,BPLCON2(a0)      ;Sprites have priority over playfields
        MOVE.W  #$0038,DDFSTRT(a0)      ;Set data-fetch start
        MOVE.W  #$00D0,DDFSTOP(a0)      ;Set data-fetch stop

; Display window definitions.

        MOVE.W  #$2C81,DIWSTRT(a0)      ;Set display window start
                                        ;Vertical start in high byte.
                                        ;Horizontal start * 2 in low byte.
        MOVE.W  #$F4C1,DIWSTOP(a0)      ;Set display window stop
                                        ;Vertical stop in high byte.
                                        ;Horizontal stop * 2 in low byte.
;
; Set up color registers.
;
        MOVE.W  #$0008,COLOR00(a0)      ;Background color = dark blue
        MOVE.W  #$0000,COLOR01(a0)      ;Foreground color = black
        MOVE.W  #$0FF0,COLOR17(a0)      ;Color 17 = yellow
        MOVE.W  #$00FF,COLOR18(a0)      ;Color 18 = cyan
        MOVE.W  #$0F0F,COLOR19(a0)      ;Color 19 = magenta
;
; Move Copper list to $20000.
;
        MOVE.L  #$20000,a1              ;Point A1 at Copper list destination
        LEA     COPPERL(pc),a2          ;Point A2 at Copper list source
CLOOP:
        MOVE.L  (a2),(a1)+              ;Move a long word
        CMP.L   #$FFFFFFFE,(a2)+        ;Check for end of list
        BNE     CLOOP                   ;Loop until entire list is moved
;
; Move sprite to $25000.
;
        MOVE.L  #$25000,a1              ;Point A1 at sprite destination
        LEA     SPRITE(pc),a2           ;Point A2 at sprite source
SPRLOOP:
        MOVE.L  (a2),(a1)+              ;Move a long word
        CMP.L   #$00000000,(a2)+        ;Check for end of sprite
        BNE     SPRLOOP                 ;Loop until entire sprite is moved
;
; Now we write a dummy sprite to $30000, since all eight sprites are activated
; at the same time and we're only going to use one.  The remaining sprites
; will point to this dummy sprite data.
;
        MOVE.L  #$00000000,$30000       ;Write it
;
; Point Copper at Copper list.
;
        MOVE.L  #$20000,COP1LC(a0)
;
; Fill bitplane with $FFFFFFFF.
;
        MOVE.L  #$21000,a1              ;Point A1 at bitplane
        MOVE.W  #1999,d0                ;2000-1(for dbf) long words = 8000 bytes
FLOOP
        MOVE.L  #$FFFFFFFF,(a1)+        ;Move a long word of $FFFFFFFF
        DBF     d0,FLOOP                ;Decrement, repeat until false.
;
; Start DMA.
;
         MOVE.W  d0,COPJMP1(a0)         ;Force load into Copper
                                        ;  program counter
        MOVE.W  #$83A0,DMACON(a0)       ;Bitplane, Copper, and sprite DMA


spin:	
	JMP spin

	RTS                             ;..return to rest of program..

;
; This is a Copper list for one bitplane, and 8 sprites.
; The bitplane lives at $21000.
; Sprite 0 lives at $25000; all others live at $30000 (the dummy sprite).
;
COPPERL:
        DC.W    BPL1PTH,$0002           ;Bitplane 1 pointer = $21000
        DC.W    BPL1PTL,$1000
        DC.W    SPR0PTH,$0002           ;Sprite 0 pointer = $25000
        DC.W    SPR0PTL,$5000
        DC.W    SPR1PTH,$0003           ;Sprite 1 pointer = $30000
        DC.W    SPR1PTL,$0000
        DC.W    SPR2PTH,$0003           ;Sprite 2 pointer = $30000
        DC.W    SPR2PTL,$0000
        DC.W    SPR3PTH,$0003           ;Sprite 3 pointer = $30000
        DC.W    SPR3PTL,$0000
        DC.W    SPR4PTH,$0003           ;Sprite 4 pointer = $30000
        DC.W    SPR4PTL,$0000
        DC.W    SPR5PTH,$0003           ;Sprite 5 pointer = $30000
        DC.W    SPR5PTL,$0000
        DC.W    SPR6PTH,$0003           ;Sprite 6 pointer = $30000
        DC.W    SPR6PTL,$0000
        DC.W    SPR7PTH,$0003           ;Sprite 7 pointer = $30000
        DC.W    SPR7PTL,$0000
        DC.W    $FFFF,$FFFE             ;End of Copper list
;
; Sprite data for spaceship sprite.  It appears on the screen at V=65 and H=128.
;
SPRITE:
        DC.W    $6D60,$7200             ;VSTART, HSTART, VSTOP
        DC.W    $0990,$07E0             ;First pair of descriptor words
        DC.W    $13C8,$0FF0
        DC.W    $23C4,$1FF8
        DC.W    $13C8,$0FF0
        DC.W    $0990,$07E0
        DC.W    $0000,$0000             ;End of sprite data

