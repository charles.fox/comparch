DMACONR		EQU		$dff002
ADKCONR		EQU		$dff010
INTENAR		EQU		$dff01c
INTREQR		EQU		$dff01e

DMACON		EQU		$dff096
ADKCON		EQU		$dff09e
INTENA		EQU		$dff09a
INTREQ		EQU		$dff09c

BPLCON0         EQU             $dff100
BPLCON1         EQU             $dff102
BPL1MOD         EQU             $dff108
BPL2MOD         EQU             $dff10a
DIWSTRT         EQU             $dff08e
DIWSTOP         EQU             $dff090
DDFSTRT         EQU             $dff092
DDFSTOP         EQU             $dff094
VPOSR           EQU             $dff004
COP1LCH         EQU             $dff080

CIAAPRA         EQU             $bfe001


init:
	; load dll graphcis libraries
	move.l	$4,a6
	move.l	#gfxname,a1
	moveq	#0,d0
	jsr	-552(a6)
	move.l	d0,gfxbase
	move.l 	d0,a6
	move.l 	34(a6),oldview
	move.l 	38(a6),oldcopper

	; calling graphics subroutines
	move.l #0,a1
	jsr -222(a6)	; LoadView
	jsr -270(a6)	; WaitTOF
	jsr -270(a6)	; WaitTOF
	move.l	$4,a6
	jsr -132(a6)	; Forbid

        ; setup displayhardware to show a 320x200px 3 bitplanes playfield
        ; with zero horizontal scroll and zero modulos
	move.w	#$3200,BPLCON0			; three bitplanes
	move.w	#$0000,BPLCON1			; horizontal scroll 0
	move.w	#$0050,BPL1MOD			; odd modulo
	move.w	#$0050,BPL2MOD			; even modulo
	move.w	#$2c81,DIWSTRT			; DIWSTRT - topleft corner (2c81)
	move.w	#$c8d1,DIWSTOP			; DIWSTOP - bottomright corner (c8d1)
	move.w	#$0038,DDFSTRT			; DDFSTRT
	move.w	#$00d0,DDFSTOP			; DDFSTOP
	move.w  #%1000000110000000,DMACON       ; DMA set ON
	move.w 	#%0000000001111111,DMACON	; DMA set OFF
	move.w 	#%1100000000000000,INTENA	; IRQ set ON
	move.w 	#%0011111111111111,INTENA	; IRQ set OFF

mainloop:
	jmp mainloop

; *******************************************************************************
; *******************************************************************************
; DATA
; *******************************************************************************
; *******************************************************************************

; storage for 32-bit addresses and data
	CNOP 0,4
oldview:	dc.l 0
oldcopper:	dc.l 0
gfxbase:	dc.l 0
frame:          dc.l 0

; storage for 16-bit data
	CNOP 0,4
olddmareq:	dc.w 0
oldintreq:	dc.w 0
oldintena:	dc.w 0
oldadkcon:	dc.w 0

; storage for 8-bit data
	CNOP 0,4
sin32_15: dc.b 8,9,10,12,13,14,14,15,15,15,14,14,13,12,10,9,8,6,5,3,2,1,1,0,0,0,1,1,2,3,5,6

	CNOP 0,4
gfxname: dc.b 'graphics.library',0

	Section ChipRAM,Data_c

	CNOP 0,4
bitplanes:
  incbin "masters3.raw"
  blk.b 320/8*3*(200-160),0

; datalists aligned to 32-bit
	CNOP 0,4
copper:
  dc.l $ffffffe
  blk.l 1023,0
