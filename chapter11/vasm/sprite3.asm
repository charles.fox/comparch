custom      equ   $dff000 ;custom chips
bplcon0     equ   $100    ;bitplane
bplcon1     equ   $102
bplcon2     equ   $104
bplpt	    equ   $0E0
bpl1mod     equ   $108
ddfstrt     equ   $092
ddfstop     equ   $094
diwstrt     equ   $08E    ;display window
diwstop     equ   $090
copjmp1     equ   $088
cop1lc	    equ   $080
dmacon	    equ   $096    ;DMA controller
sprpt	    equ   $120    ;sprite pointer
color	    equ   $180    ;color table

COLOR00     equ   color+$00
COLOR01     equ   color+$02
COLOR17     equ   color+$22
COLOR18     equ   color+$24
COLOR19     equ   color+$26
BPL1PTH     equ   bplpt+$00
BPL1PTL     equ   bplpt+$02
SPR0PT      equ   sprpt+$00
SPR0PTH     equ   SPR0PT+$00
SPR0PTL     equ   SPR0PT+$02
SPR1PT      equ   sprpt+$04
SPR1PTH     equ   SPR1PT+$00
SPR1PTL     equ   SPR1PT+$02
SPR2PT      equ   sprpt+$08
SPR2PTH     equ   SPR2PT+$00
SPR2PTL     equ   SPR2PT+$02
SPR3PT      equ   sprpt+$0C
SPR3PTH     equ   SPR3PT+$00
SPR3PTL     equ   SPR3PT+$02
SPR4PT      equ   sprpt+$10
SPR4PTH     equ   SPR4PT+$00
SPR4PTL     equ   SPR4PT+$02
SPR5PT      equ   sprpt+$14
SPR5PTH     equ   SPR5PT+$00
SPR5PTL     equ   SPR5PT+$02
SPR6PT      equ   sprpt+$18
SPR6PTH     equ   SPR6PT+$00
SPR6PTL     equ   SPR6PT+$02
SPR7PT      equ   sprpt+$1C
SPR7PTH     equ   SPR7PT+$00
SPR7PTL     equ   SPR7PT+$02

;set up a single bitplane.
        lea     custom,a0               ;Point a0 at custom chips
        move.w  #$1200,bplcon0(a0)      ;1 bitplane color is on
        move.w  #$0000,bpl1mod(a0)      ;Modulo = 0
        move.w  #$0000,bplcon1(a0)      ;Horizontal scroll value = 0
        move.w  #$0024,bplcon2(a0)      ;Sprites have priority over playfields
        move.w  #$0038,ddfstrt(a0)      ;Set data-fetch start
        move.w  #$00D0,ddfstop(a0)      ;Set data-fetch stop

; Display window definitions.
        move.w  #$2c81,diwstrt(a0)      ;Set display window start
                                        ;Vertical start in high byte.
                                        ;Horizontal start * 2 in low byte.
        move.w  #$f4c1,diwstop(a0)      ;Set display window stop
                                        ;Vertical stop in high byte.
                                        ;Horizontal stop * 2 in low byte.
; Set up color registers.
        move.w  #$0008,COLOR00(a0)      ;Background color = dark blue
        move.w  #$0000,COLOR01(a0)      ;Foreground color = black
        move.w  #$0ff0,COLOR17(a0)      ;Color 17 = yellow
        move.w  #$00ff,COLOR18(a0)      ;Color 18 = cyan
        move.w  #$0f0f,COLOR19(a0)      ;Color 19 = magenta

; Move Copper list to $20000.
        move.l  #$20000,a1              ;Point A1 at Copper list destination
        lea     copperl(pc),a2          ;Point A2 at Copper list source
cloop:
        move.l  (a2),(a1)+              ;Move a long word
        cmp.l   #$fffffffe,(a2)+        ;Check for end of list
        bne     cloop                   ;Loop until entire list is moved

; Move sprite to $25000.
        move.l  #$25000,a1              ;Point A1 at sprite destination
        lea     sprite(pc),a2           ;Point A2 at sprite source
sprloop:
        move.l  (a2),(a1)+              ;Move a long word
        cmp.l   #$00000000,(a2)+        ;Check for end of sprite
        bne     sprloop                 ;Loop until entire sprite is moved

; Now we write a dummy sprite to $30000, since all eight sprites are activated
; at the same time and we're only going to use one.  The remaining sprites
; will point to this dummy sprite data.
        move.l  #$00000000,$30000       ;Write it

; Point Copper at Copper list.
        move.l  #$20000,cop1lc(a0)

; Fill bitplane with $ffffffff.
        move.l  #$21000,a1              ;Point A1 at bitplane
        move.w  #1999,d0                ;2000-1(for dbf) long words = 8000 bytes
floop:
        move.l  #$ffffffff,(a1)+        ;Move a long word of $ffffffff
        dbf     d0,floop                ;Decrement, repeat until false.

; Start DMA.
        move.w  d0,copjmp1(a0)          ;Force load into Copper program counter
        move.w  #$83A0,dmacon(a0)       ;Bitplane, Copper, and sprite DMA

spin:	
	jmp spin

; This is a Copper list for one bitplane, and 8 sprites.
; The bitplane lives at $21000.
; Sprite 0 lives at $25000; all others live at $30000 (the dummy sprite).
copperl:
        dc.w    BPL1PTH,$0002           ;Bitplane 1 pointer = $21000
        dc.w    BPL1PTL,$1000
        dc.w    SPR0PTH,$0002           ;Sprite 0 pointer = $25000
        dc.w    SPR0PTL,$5000
        dc.w    SPR1PTH,$0003           ;Sprite 1 pointer = $30000
        dc.w    SPR1PTL,$0000
        dc.w    SPR2PTH,$0003           ;Sprite 2 pointer = $30000
        dc.w    SPR2PTL,$0000
        dc.w    SPR3PTH,$0003           ;Sprite 3 pointer = $30000
        dc.w    SPR3PTL,$0000
        dc.w    SPR4PTH,$0003           ;Sprite 4 pointer = $30000
        dc.w    SPR4PTL,$0000
        dc.w    SPR5PTH,$0003           ;Sprite 5 pointer = $30000
        dc.w    SPR5PTL,$0000
        dc.w    SPR6PTH,$0003           ;Sprite 6 pointer = $30000
        dc.w    SPR6PTL,$0000
        dc.w    SPR7PTH,$0003           ;Sprite 7 pointer = $30000
        dc.w    SPR7PTL,$0000
        dc.w    $ffff,$fffe             ;End of Copper list

; Sprite data for spaceship sprite.  
;It appears on the screen at V=65 and H=128.
sprite:
        dc.w    $6d60,$7200             ;VSTART, HSTART, VSTOP
        dc.w    $0990,$07e0             ;First pair of descriptor words
        dc.w    $13c8,$0ff0
        dc.w    $23c4,$1ff8
        dc.w    $13c8,$0ff0
        dc.w    $0990,$07e0
        dc.w    $0000,$0000             ;End of sprite data

