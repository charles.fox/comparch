import re

fn = "kernel5_neuron_manual.sass"

polarity=True
d=dict()

for line in open(fn):
    line=line.strip()

    line=re.sub("\*/", "", line)
    line=re.sub("/\*", "", line)
    line=re.sub("0x", "", line)
    line=re.sub("                   ", " ", line)

    e=line
#    e = line[0:4] + line[18:]


    if polarity:
        d[1]=e
    else:
        newline = d[1] + e
        
        print(newline)
    polarity=not polarity
