__global__ void myKernel(double *x, double *w, double *out)
{
    int id = threadIdx.x;   //get my ID
    out[id] = x[id] + w[id];
}


