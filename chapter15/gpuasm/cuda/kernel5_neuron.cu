__global__ void myKernel(double *x, double *w, double *out)
{
    int id = threadIdx.x;   //get my ID
    double sum=0;
    double term=0;
    for (int i=0; i<100; i++) {
    	term = w[i]*x[id+i];
    	sum = sum + term;
    } 
    //f=reLU
    if (sum>0) {
	    out[id] = sum;
    } else {
	    out[id] = 0;
    }	         
}


