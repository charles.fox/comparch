//!nvcc  -arch=sm_75 -ptx kernel.cu
//!ptxas -arch=sm_75 "kernel.ptx" -o "kernel.cubin"
//!nvcc myptxhost.cu -lcuda
//./a.out

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cuda.h"
#include <iostream>
using namespace std;
 
int main( int argc, char* argv[] )
{
    //load the cubin executable (assembled from the PTX)
    cuInit(0);
    CUcontext pctx;
    CUdevice dev;
    cuDeviceGet(&dev, 0);
    cuCtxCreate(&pctx, 0, dev);
    CUmodule module;
    CUfunction vector_add;
    const char* module_file = "kernel.cubin";
    int err;
    err = cuModuleLoad(&module, module_file);
    if( err != CUDA_SUCCESS ) {
        std::cout << "Failed to load self fatbin : " << err << std::endl;
        return -1;
    }
    const char* kernel_name = "_Z8myKernelPdS_S_";
    err = cuModuleGetFunction(&vector_add, module, kernel_name);
    if (err != CUDA_SUCCESS) {
            fprintf(stderr, "* Error getting kernel function %i\n", err);
            exit(-1);
    }
    
    int n = 100000;   // Size of vectors
    double *h_x, *h_w, *h_out;      // Host input and output vectors
    double *d_x, *d_w, *d_out;      // Device input and output vectors
    size_t bytes = n*sizeof(double);     // Size, in bytes, of each vector
 
    h_x = (double*)malloc(bytes);  // Allocate memory for vectors on host
    h_w = (double*)malloc(bytes);
    h_out = (double*)malloc(bytes);
 
    cudaMalloc(&d_x, bytes);      // Allocate memory for each vector on GPU
    cudaMalloc(&d_w, bytes);
    cudaMalloc(&d_out, bytes);
 
    int i;                         // Initialize vectors on host to some arbitary values
    for( i = 0; i < n; i++ ) {
        h_x[i] = sin(i)*sin(i);
        h_w[i] = cos(i)*cos(i);
    }
    cudaMemcpy( d_x, h_x, bytes, cudaMemcpyHostToDevice);  // Copy host vectors to device
    cudaMemcpy( d_w, h_w, bytes, cudaMemcpyHostToDevice);
  
    //set arguments and lauch the kernels on the GPU
    int blockSize, gridSize;    // Number of threads in block, and blocks in grid
    blockSize = 1024;
    gridSize = (int)ceil((float)n/blockSize);
    void *args[3] = { &d_x , &d_w, &d_out  };
    cuLaunchKernel(vector_add,  gridSize,1,1,     // Nx1x1 blocks
                                blockSize,1,1,    // 1x1x1 threads
                                0, 0, args, 0 );
 
    cudaMemcpy( h_out, d_out, bytes, cudaMemcpyDeviceToHost );  // Copy result to host
    for(i=0; i<10; i++)                   //print result
      printf("out: %f\n", h_out[i]);
    cudaFree(d_x);  cudaFree(d_w); cudaFree(d_out);     // Release device memory
    free(h_x); free(h_w); free(h_out);   // Release host memory
    return 0;
}

