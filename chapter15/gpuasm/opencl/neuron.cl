
__kernel void neuron(  __global float *x,                       
                       __global float *w,                      
                       __global float *out,                      
                       const unsigned int n)                    
{                                                                                                
    int id = get_global_id(0);                                               
    
    float sum = 0;
    for (int i=0; i<10; i++) 
    {
     sum=sum+w[i]*x[id+i];
    }
    if (sum>0)   //f = reLU
    {
      out[id] = sum;
    } else {
      out[id] = 0.0f;
    }    
}       

