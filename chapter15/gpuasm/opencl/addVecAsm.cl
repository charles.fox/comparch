
__kernel void addVecAsm(__global float *a,
                        __global const float *b,
                        __global float *c,
                        const unsigned int n)
{
  int gid = get_global_id(0);
  float out;

  asm(".reg .pred 	%p<2>;\\
	.reg .f32 	%f<4>;\\
	.reg .b32 	%r<4>;\\
	.reg .b64 	%rd<10>;\\
	mov.u32 	%r1, 0;\\
	{ // callseq 0, 0\\
	.reg .b32 temp_param_reg;\\
	.param .b32 param0;\\
	st.param.b32 	[param0+0], %r1;\\
	.param .b64 retval0;\\
	call.uni (retval0), \\
	_Z13get_global_idj, \\
	(\\
	param0\\
	);\\
	ld.param.b64 	%rd5, [retval0+0];\\
	} // callseq 0\\
	cvt.u32.u64 	%r2, %rd5;\\
	ld.param.u32 	%r3, [addVec_param_3];\\
	setp.ge.u32 	%p1, %r2, %r3;\\
	@%p1 bra 	LBB0_2;\\
	ld.param.u64 	%rd4, [addVec_param_0];\\
	ld.param.u64 	%rd6, [addVec_param_1];\\
	ld.param.u64 	%rd7, [addVec_param_2];\\
	shl.b64 	%rd8, %rd5, 32;\\
	shr.s64 	%rd9, %rd8, 30;\\
	add.s64 	%rd1, %rd7, %rd9;\\
	add.s64 	%rd2, %rd6, %rd9;\\
	add.s64 	%rd3, %rd4, %rd9;\\
	ld.global.f32 	%f1, [%rd3];\\
	ld.global.f32 	%f2, [%rd2];\\
	add.rn.f32 	%f3, %f1, %f2;\\
	st.global.f32 	[%rd1], %f3;\\
LBB0_2:\\
	ret;\\
      " : "=r"(out)); 
	
  a[gid] = out;
}


