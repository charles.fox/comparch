
__kernel void addVecAsm(__global float *a,
                        __global const float *b,
                        __global float *c,
                        const unsigned int n)
{
  int gid = get_global_id(0);
  float out;

  asm(".reg .b32 	r1, r2;   \\  
       .reg .b64 	rd1, rd2; \\
        ld.param.u64 	rd1, [vecAddPtx_param_2]; \\
        mov.u32 r1, 0; \\        
        st.global.u32 [rd1], r1; \\
        ret; \\
      " : "=r"(out)); 
	
  a[gid] = out;
}

