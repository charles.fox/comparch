#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <CL/opencl.h>
 
int main( int argc, char* argv[] )
{
    float *h_x;                       // Host input vectors
    float *h_w;
    float *h_out;                     // Host output vector
    cl_mem d_x;                       // Device input buffers
    cl_mem d_w;
    cl_mem d_out;                     // Device output buffer
    cl_platform_id cpPlatform;        // OpenCL platform
    cl_device_id device_id;           // device ID
    cl_context context;               // context
    cl_command_queue queue;           // command queue
    cl_program program;               // program
    cl_kernel kernel;                 // kernel

    unsigned int n = 100000;          //max vector length   
    size_t bytes = n*sizeof(float);   //size of vectors in bytes
    h_x = (float*)malloc(bytes);      //host memory
    h_w = (float*)malloc(bytes);
    h_out = (float*)malloc(bytes);
 
    int i;       // Initialize vectors with some data
    for( int i = 0; i < n; i++ ) {
        h_x[i] = float(i); 
        h_w[i] = float(i)/2; 
    }
 
    size_t globalSize, localSize;
    localSize = 64;     // Number of work items in each local work group (warpsize)
    globalSize = 128;   //Total num of work items (MUST be multiple of localSize)

    cl_int err;
    err = clGetPlatformIDs(1, &cpPlatform, NULL);
    err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    queue = clCreateCommandQueue(context, device_id, 0, &err);
 
    //load the openCL kernel source code as a string
    FILE *fp;
    char *source_str;
    size_t source_size, program_size;
    fp = fopen("neuron.cl", "rb");
    if (!fp) {
        printf("Failed to load kernel\n");
        return 1;
    }
    fseek(fp, 0, SEEK_END);
    program_size = ftell(fp);
    rewind(fp);
    source_str = (char*)malloc(program_size + 1);
    source_str[program_size] = '\0';
    fread(source_str, sizeof(char), program_size, fp);
    fclose(fp);

    // Compile and assemble the program from the source
    program = clCreateProgramWithSource(context, 1,
                            (const char **) & source_str, NULL, &err); 
    clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    kernel = clCreateKernel(program, "neuron", &err);   //which kernel to run
 
    // Create the input and output arrays in device memory for our calculation
    d_x = clCreateBuffer(context, CL_MEM_READ_ONLY, bytes, NULL, NULL);
    d_w = clCreateBuffer(context, CL_MEM_READ_ONLY, bytes, NULL, NULL);
    d_out = clCreateBuffer(context, CL_MEM_WRITE_ONLY, bytes, NULL, NULL);
 
    // Write our data set into the input array in device memory
    err = clEnqueueWriteBuffer(queue, d_x, CL_TRUE, 0,
                                   bytes, h_x, 0, NULL, NULL);
    err |= clEnqueueWriteBuffer(queue, d_w, CL_TRUE, 0,
                                   bytes, h_w, 0, NULL, NULL);
 
    // Set the arguments to the compute kernel, then run it in parallel
    err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_x);
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_w);
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_out);
    err |= clSetKernelArg(kernel, 3, sizeof(unsigned int), &n);
    err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, 
                                &globalSize, &localSize, 0, NULL, NULL);
    clFinish(queue);              //wait for all jobs to complete
 
    // Read the results from the device, and print them out
    clEnqueueReadBuffer(queue, d_out, CL_TRUE, 0, bytes, h_out, 0, NULL, NULL ); 
    for(i=0; i<20; i++)  printf("out: %f\n", h_out[i]);  
 
    clReleaseMemObject(d_x);     // release OpenCL resources
    clReleaseMemObject(d_w);
    clReleaseMemObject(d_out);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);    
    free(h_x);                  //release host memory
    free(h_w);
    free(h_out);
    return 0;
}


