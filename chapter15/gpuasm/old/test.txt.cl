!pip install pyopencl
!pip install pillow
from __future__ import division
import numpy as np
import pyopencl as cl
import pyopencl.array
from pyopencl.clrandom import rand as clrand
import pyopencl.cltypes as cltypes
from PIL import Image
from IPython.display import display
%load_ext pyopencl.ipython_ext
print(pyopencl.get_platforms())
ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

---

%%cl_kernel

__kernel void sum_vector(__global float4 *a, __global const float4 *b)
{
  int gid = get_global_id(0);
  
  a[gid].x = 66;

  a[gid].y = fmod((a[gid].y + b[gid].y * 1.0),1000.0);
 
 
  //This command gets the lane ID within the current warp
  int laneid;
  asm("mov.u32 %0, %%laneid;" : "=r"(laneid));
  a[gid].z = laneid;

}


----


n = 10_000_000

a = cl.array.zeros(queue, n, dtype=cltypes.float4)
#a = clrand(queue, (n,), dtype=cltypes.float4, a=0.0, b=1000.0)

b_host = np.random.randn(n).astype(cltypes.float4)
b = cl.array.to_device(queue, b_host)
#c = cl.array.zeros(queue, n, dtype=cltypes.float4)

sum_vector(queue, (n,), None, a.data, b.data)
print(a.get())








===============

NB this doesnt work:   .reg .b32 	%r<2>;
but this does:         .reg .b32 	r1, r2; 


then copy all the asm from inside the {}s into inline.

NB params are unsigned pointers to float arrays, not floats


//FP rep for 1.0:
 mov.f32 r2, 0F3f800000







__kernel void vecAdd(  __global float *a,                       
                       __global float *b,                      
                       __global float *c,                      
                       const unsigned int n)                    
{                                                                                                
    c[0]=id;

}       


-->

%%cl_kernel

__kernel void vecAddPtx(__global float *a,
                        __global const float *b,
                        __global float *c,
                        const unsigned int n)
{
  int gid = get_global_id(0);
  float out;

  asm(".reg .b32 	r1, r2;   \\  
       .reg .b64 	rd1, rd2; \\
        ld.param.u64 	rd1, [vecAddPtx_param_2]; \\
        mov.u32 r1, 0; \\        
        st.global.u32 [rd1], r1; \\
        ret; \\
      " : "=r"(out)); 
	
  a[gid] = out;
}

