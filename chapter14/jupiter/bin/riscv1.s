.globl __start
__start:
addi x1, x0, 3 ; load immediate integer 3 to x1
add x2, x1, x0 ; copy x1 to x2