These are examples of digital logic designs exported from LogiSim Evolution and used to make silicon mask designs, which can be manufactured.
Images include a single adder, and the Manchester Baby from the book
Details of how to do this process, including actually getting your own chips fabricated at low cost, will appear in a forthcoming paper by Kristaps Jurkans and Charles Fox.
The export process was figured out by Kristaps Jurkans as part of his MRes research at the University of Lincoln.
