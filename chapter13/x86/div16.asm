;nasm -f elf64 div16.asm 
;ld -s -o div16 div16.o
;./div16
;echo $?

section .data 
hello: db 'Hello world!',10 ; 'Hello world!' plus a linefeed character


helloLen: equ $-hello ; Length of the 'Hello world!' string
; (I'll explain soon)
section .text
global _start
_start:
mov eax,4 ; The system call for write (sys_write)
mov ebx,1 ; File descriptor 1 - standard output
mov ecx,hello ; Put the offset of hello in ecx
mov edx,helloLen ; helloLen is a constant, so we don't need to say
; mov edx,[helloLen] to get it's actual value
int 80h ; Call the kernel

mov ax, -48   ; dividend
cwd
mov bx, 5    ; divisor
idiv bx       ; result stored in AL


mov rbx, rax  ;exit codei goes in rbx
mov rax,1 ; The system call for exit (sys_exit)
int 80h


