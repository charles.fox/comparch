;$ nasm -felf64 test.asm  ;  gcc -no-pie -o test  test.o ; ./test

global  main
extern printf    

section .data
msg: db "Hello libC!", 0
fmt: db "%s", 10, 0 
fmtint: db '%10d', 10, 0

myvar: dq 0fffffh
myarray: dq 1,2,3,4,5
myzeros: times 4 dw 0

x: dq 3
y: dq 2


a: dq 1.456  ;floats
b: dq 2.381


section .bss
c: resq 1   ; reserve for output


section .text
main:
	;printing hello
	mov	rdi,fmt
	mov	rsi,msg
	mov	rax,0		; or can be  xor  rax,rax
        call    printf		; Call C function
	

	;TESTING

	;immediate addressing
	mov	rbx,123
	mov	ebx,4c6h
	mov 	bh,01101100b


	;direct addressing
	mov 	rbx, myvar    ;puts the address myvar in the reg
	mov 	rbx, [myvar]    ;puts the CONTENTS of myvar in the reg
	mov	[myvar], rbx     ;fails unless in section .data 

	mov 	rdi, myvar
	mov	[rdi], rax
	mov	rax, [rdi]


		; c=a+b;
	fld	qword [a] 	; load a (pushed on flt pt stack, st0)
	fadd	qword [b]	; floating add b (to st0)
	fstp	qword [c]	; store into c (pop flt pt stack)



	fld	qword [a]	; load a (pushed on flt pt stack, st0)
	fmul	qword [b]	; floating multiply by b (to st0)
	fstp	qword [c]	; store product into c (pop flt pt stack)

	;and rax, [x]



	;register addressing
	mov	rsi, rax

	;print result
	mov	rdi,fmtint
;	mov	rsi,124
	mov	rax,0		; or can be  xor  rax,rax
        call    printf		; Call C function

