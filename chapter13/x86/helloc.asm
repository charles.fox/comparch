global main
extern printf    

msg: db "Hello libC!", 0   ; 0=ASCII endofstring
fmtstr: db "%s", 10, 0     ; ASCII newline and endofstring
fmtint: db '%10d', 10, 0   ; ASCII newline and endofstring

main:
    mov	rdi,fmtstr
    mov	rsi,msg     ; pointer to msg 
    mov	rax,0       ; num of extra stack args used (none)
    call printf     ; call C function
	
    mov	rdi,fmtint
    mov	rsi,124     ; 124 is an int to print out
    mov	rax,0       ; num of extra stack args used (none)
    call printf     ; call C function
    ret
