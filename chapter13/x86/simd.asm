;$ nasm -felf64 simd.asm  ;  gcc -no-pie -o simd  simd.o ; ./simd

global  main
extern printf    

section .data
msg: 	db "Hello libC!", 0
fmt: 	db "%s", 10, 0 
fmtint: db '%10d',10,0

a:	dd  4, 3 
b:	dd  1, 5 
c:	dd  0,0

section .text
main:
	mov	rdi,fmt
	mov	rsi,msg
	mov	rax,0		; or can be  xor  rax,rax
        call    printf		; Call C function
	
	mov	rdi,fmtint
	mov	rsi,124
	mov	rax,0		; or can be  xor  rax,rax
        call    printf		; Call C functi

	movq	mm0, [a]
	movq	mm1, [b]
	paddd	mm0, mm1
	movq    [c], mm0	;only the first d is copied, not the 2nd?	

	mov	rdi,fmtint
	mov	rsi,[c]
	mov	rax,0		; or can be  xor  rax,rax
        call    printf		; Call C functi


