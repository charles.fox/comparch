;nasm -f elf64 hellok.asm 
;ld -s -o hellok hellok.o
;./hellok
;echo $?     #displays the exit code returned in ebx



          global    _start

_start:   mov       rax, 1                  ; system call for write
          mov       rdi, 1                  ; file handle 1 is stdout
          mov       rsi, message            ; address of string to output
          mov       rdx, 13                 ; number of bytes
          syscall                           ; invoke OS to do the write
          mov       rax, 60                 ; system call for exit
          xor       rdi, rdi                ; exit code 0
          syscall                           ; invoke operating system to exit
	  ret

message:  db        "Hello, Kernel!", 10    ; note the newline at the end
